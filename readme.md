# Loja Virtual em MVC
Projeto simples de uma loja virtual utilizando o padrão MVC em PHP7.

## Pré-Requisitos
`php >= 7.4.x |
apache >= 2.4 |
composer >= 2.0.11 |
mariadb >= 15.1`

## Instalação
##### Habilitar mod_rewrite no apache para que o *.htaccess* redirecione as rotas

```aen2mod rewrite```
##### Também modificar o arquivo `etc/apache2/apache2.conf`
#
```
<Directory /dir/to/project/>
	Options Indexes FollowSymLinks
	AllowOverride All
	Require all granted
</Directory>
```

##### Instalar dependencias do composer e carregar o autoload
``` composer install```
``` composer dump-autoload -o```

# Rotas disponíveis

 **/dashboard -  GET**

 **/product - GET**

 **/addProduct - GET/POST/PUT/DELETE**

 >Parametros:
    ```POST:{
        'name': string, [optional]
        'sku': string, - [optional]
        'price': float, - [optional]
        'description': string, - [optional]
        'quantity': integer, - [optional]
        'category': array, - [optional]
    }```
    ```PUT/DELETE: {
        'id': integer, [required]
        'name': string, [optional]
        'sku': string, - [optional]
        'price': float, - [optional]
        'description': string, - [optional]
        'quantity': integer, - [optional]
        'category': array, - [optional]
    }```

 **/categories - GET**

 **/addCategories - GET/POST/PUT/DELETE**

 >Parametros:
    ```POST:{
        'category-name': string, [required]
        'category-code': string, [required]
    }```
    ```PUT/DELETE: {
        'id': integer, [required]
        'category-name': string, [optional]
        'category-code': string, [optional]
    }```
