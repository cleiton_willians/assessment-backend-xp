<?php

require_once('./vendor/autoload.php');

use Core\Application as Application;
use Core\Logger as Logger;

try {
    Application::run();
} catch (Exception $e) {
    $logger = Logger::getLogger('application');
    $logger->error('Application error: ' . $e->getMessage());
}
