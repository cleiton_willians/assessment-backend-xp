<?php

namespace Model;

use Core\Connection as Connection;
use Core\Logger as Logger;
/**
 *
 */
class BaseModel
{
    protected $conn;
    protected $config;
    protected $tablename;
    private $logger;

    function __construct()
    {
        $this->config = include(CONFIG_PATH . 'config.php');
        $this->conn = Connection::getConnection();
        $this->logger = new Logger('mariadb');
    }

    public function getTables()
    {
        $dbname = $this->config['mariadb']['dbname'];
        $query = 'SHOW TABLES FROM ' . $dbname;
        $result = \mysqli_query($this->conn, $query);
        return \mysqli_fetch_array($result);
    }

    public function listAll(): array
    {
        $query = "SELECT * FROM {$this->tablename};";
        $stmt = $this->conn->prepare($query);
        $returnArray = [];

        if ($stmt->execute()) {
            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                $returnArray[] = $row;
            }
        }

        return $returnArray;
    }

    public function insert($data)
    {
        if ($this->isAssoc($data)) {
            // Adding quotes to values if not numeric
            $values = array_map(function ($str) {
                return !is_numeric($str) ? sprintf("'%s'", $str) : $str;
            }, array_values($data));

            $query = "INSERT INTO {$this->tablename} (".
                implode(', ', array_keys($data)).
                ") VALUES (".
                implode(', ', $values).");";
            $stmt = $this->conn->prepare($query);

            if ($stmt->execute()) {
                $this->logger->info("Insert successful: " . $query);
                return true;
            } else {
                $this->logger->error("Insert failed: " . $stmt->errorInfo());
                return false;
            }
        }
    }

    public function update($data)
    {
        if ($this->isAssoc($data) && isset($data['id']) && !empty($data['id'])) {
            $query = 'UPDATE ' . $this->tablename;
            $set = $this->prepareSet($data);
            $where = " WHERE id = {$data['id']};";
            $query = $query . rtrim($set, ', ') . $where;
            $stmt = $this->conn->prepare($query);

            if ($stmt->execute()) {
                $this->logger->info("Update successful: " . $query);
                return true;
            } else {
                $this->logger->error("Update failed: " . $stmt->errorInfo());
                return false;
            }
        } else {
            $this->logger->error("Update failed! Dataset: " . $data);
            return false;
        }
    }

    public function delete($id)
    {
        $query = "DELETE FROM {$this->tablename} WHERE id = {$id};";
        $stmt = $this->conn->prepare($query);

        if ($stmt->execute()) {
            $this->logger->info("Delete successful: " . $query);
            return true;
        } else {
            $this->logger->error("Delete failed: " . $stmt->errorInfo());
            return false;
        }
    }

    protected function isAssoc(array $data)
    {
        foreach ($data as $key => $value) {
            if (is_string($key)) {
                return true;
            } else {
                return false;
            }
        }
    }

    protected function prepareSet(array $data)
    {
        unset($data['id']);
        $set = ' SET';

        foreach ($data as $coll => $value) {
            $set .= " {$coll} = '{$value}', ";
        }

        return $set;
    }

}
