<?php

namespace View;

/**
 *
 */
class BaseView
{
    protected $mainContent;

    public function render()
    {
        $header = 'view/_template/header.php';
        $footer = 'view/_template/footer.php';
        ob_start();
        include(APP_PATH . $header);
        echo($this->mainContent);
        include(APP_PATH . $footer);
        $output = ob_get_clean();
        ob_flush();
        echo $output;
    }
}
