<?php

namespace View;

/**
 *
 */
class DashboardView extends BaseView
{
    public $mainContent;

    function __construct($data)
    {
        $this->mainContent = $this->upperContent();
        $this->mainContent .= $this->middleContent($data);
        $this->mainContent .= $this->lowerContent();
    }

    protected function upperContent()
    {
        return '
        <!-- Main Content -->
        <main class="content">
          <div class="header-list-page">
            <h1 class="title">Dashboard</h1>
          </div>';
    }

    protected function lowerContent()
    {
        return '
        </ul>
          </main>
          <!-- Main Content -->
        ';
    }

    protected function middleContent($data)
    {
        $content = '
        <div class="infor">
          You have ' . count($data) . ' products added on this store: <a href="addProduct" class="btn-action">Add new Product</a>
        </div>
        <ul class="product-list">';

        foreach ($data as $d) {
            $content .= '
                <li>
                  <div class="product-image">
                    <!-- <img src="public/assets/images/product/tenis-runner-bolt.png" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" /> -->
                  </div>
                  <div class="product-info">
                    <div class="product-name"><span>'. $d['nome'] .'</span></div>
                    <div class="product-price"><span class="special-price">' . $d['quantidade'] . ' available</span> <span>R$' . $d['preco'] . '</span></div>
                  </div>
                </li>
            ';
        }

        return $content;
    }

}
