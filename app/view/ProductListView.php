<?php
namespace View;

/**
 *
 */
class ProductListView extends BaseView
{
    protected $mainContent;

    function __construct($data)
    {
        $this->mainContent = $this->upperContent();
        $this->mainContent .= $this->middleContent($data);
        $this->mainContent .= $this->lowerContent();
    }

    protected function upperContent()
    {
        return '
        <!-- Main Content -->
        <main class="content">
          <div class="header-list-page">
            <h1 class="title">Products</h1>
            <a href="addProduct" class="btn-action">Add new Product</a>
          </div>
          <table class="data-grid">
        ';
    }

    protected function lowerContent()
    {
        return '
            </table>
          </main>
          <!-- Main Content -->
        ';
    }

    protected function middleContent($data)
    {
        foreach ($data as $d)
        {
            $this->mainContent .= '
            <tr class="data-row">
              <td class="data-grid-td">
                 <span class="data-grid-cell-content">'. $d['nome'] .'</span>
              </td>

              <td class="data-grid-td">
                 <span class="data-grid-cell-content">' . $d['sku'] . '</span>
              </td>

              <td class="data-grid-td">
                 <span class="data-grid-cell-content">R$ ' . $d['preco'] . '</span>
              </td>

              <td class="data-grid-td">
                 <span class="data-grid-cell-content">'. $d['quantidade'] .'</span>
              </td>
              <td class="data-grid-td">';
            $categories = explode("|", $d['categoria']);
            foreach ($categories as $cat) {
                $this->mainContent .=
                '
                <span class="data-grid-cell-content">' . $cat . '</br></span>';
            }

            $this->mainContent .= '
            </td>
              <td class="data-grid-td">
                <div class="actions">
                  <div class="action edit"><span>Edit</span></div>
                  <div class="action delete"><span>Delete</span></div>
                </div>
              </td>
            </tr>
            ';
        }
    }
}
