<?php

namespace View;

/**
 *
 */
class AddCategoryView extends BaseView
{
    public $mainContent;

    function __construct()
    {
        $this->mainContent = $this->middleContent();
    }

    protected function middleContent()
    {
        $content = '
            <!-- Main Content -->
            <main class="content">
                <h1 class="title new-item">New Category</h1>

                <form method="post">
                    <div class="input-field">
                        <label for="category-name" class="label">Category Name</label>
                        <input type="text" name="category-name" class="input-text" />
                    </div>
                    <div class="input-field">
                        <label for="category-code" class="label">Category Code</label>
                        <input type="text" name="category-code" class="input-text" />
                    </div>
                    <div class="actions-form">
                        <a href="categories" class="action back">Back</a>
                        <input class="btn-submit btn-action"  type="submit" value="Save" />
                    </div>
                </form>
            </main>
            <!-- Main Content -->
        ';

        return $content;
    }
}
