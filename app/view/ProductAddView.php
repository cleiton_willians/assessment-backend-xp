<?php
namespace View;

/**
 *
 */
class ProductAddView extends BaseView
{
    public $mainContent;

    function __construct($data)
    {
        $this->mainContent .= $this->upperContent();
        $this->mainContent .= $this->middleContent($data);
        $this->mainContent .= $this->lowerContent();
    }

    public function upperContent()
    {
        return '
            <!-- Main Content -->
            <main class="content">
                <h1 class="title new-item">New Product</h1>

                <form method="post">
                    <div class="input-field">
                        <label for="sku" class="label">Product SKU</label>
                        <input type="text" name="sku" class="input-text" />
                    </div>
                    <div class="input-field">
                        <label for="name" class="label">Product Name</label>
                        <input type="text" name="name" class="input-text" />
                    </div>
                    <div class="input-field">
                        <label for="price" class="label">Price</label>
                        <input type="text" name="price" class="input-text" />
                    </div>
                    <div class="input-field">
                        <label for="quantity" class="label">Quantity</label>
                        <input type="text" name="quantity" class="input-text" />
                    </div>
                    <div class="input-field">
                        <label for="category" class="label">Categories</label>
                        <select multiple="multiple" name="category[]" class="input-text">
        ';
    }

    public function middleContent($data)
    {
        $content = '';

        foreach ($data as $d) {
            $content .= '
            <option>
                ' . $d['nome'] . '
            </option>';
        }
        return $content;
    }

    public function lowerContent()
    {
        return '
                            </select>
                        </div>
                    <div class="input-field">
                        <label for="description" class="label">Description</label>
                        <textarea name="description" class="input-text"></textarea>
                    </div>
                    <div class="actions-form">
                        <a href="products" class="action back">Back</a>
                        <input class="btn-submit btn-action" type="submit" value="Save Product" />
                    </div>
                </form>
            </main>
            <!-- Main Content -->
        ';
    }

}
