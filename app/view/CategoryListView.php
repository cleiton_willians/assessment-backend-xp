<?php

namespace View;
/**
 *
 */
class CategoryListView extends BaseView
{
    public $mainContent;

    function __construct($data)
    {
        $this->mainContent = $this->upperContent();
        $this->mainContent .= $this->middleContent($data);
        $this->mainContent .= $this->lowerContent();
    }

    protected function upperContent()
    {
        return '
        <!-- Main Content -->
        <main class="content">
          <div class="header-list-page">
            <h1 class="title">Categories</h1>
            <a href="addCategory" class="btn-action">Add new Category</a>
          </div>
          <table class="data-grid">
            <tr class="data-row">
              <th class="data-grid-th">
                  <span class="data-grid-cell-content">Name</span>
              </th>
              <th class="data-grid-th">
                  <span class="data-grid-cell-content">Code</span>
              </th>
              <th class="data-grid-th">
                  <span class="data-grid-cell-content">Actions</span>
              </th>
            </tr>';
    }

    protected function lowerContent()
    {
        return '
                </table>
            </main>
            <!-- Main Content -->';
    }

    protected function middleContent($data)
    {
        $content = '';
        foreach ($data as $d) {
            $content .= '
                <tr class="data-row">
                    <td class="data-grid-td">
                        <span class="data-grid-cell-content">' . $d['nome'] . '</span>
                    </td>

                    <td class="data-grid-td">
                        <span class="data-grid-cell-content">' . $d['codigo'] . '</span>
                    </td>

                    <td class="data-grid-td">
                        <div class="actions">
                            <div class="action edit"><span>Edit</span></div>
                            <div class="action delete"><span>Delete</span></div>
                        </div>
                    </td>
                </tr>
            ';
        }
        return $content;
    }
    
}
