<?php

namespace Controller;

use Model\Products as Products;
use Model\Categories as Categories;
use View\ProductListView as ProductListView;
use View\ProductAddView as ProductAddView;
use Core\Logger as Logger;
/*
*
*/

class ProductController
{

        public function indexAction()
        {
            $produtos = new Products();
            $data = $produtos->listAll();
            $view = new ProductListView($data);

            $view->render();
        }

        public function addAction()
        {
            if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                $categories = new Categories();
                $data = $categories->listAll();
                $productsAddView = new ProductAddView($data);
                $productsAddView->render();
            }
            $logger = Logger::getLogger('application');

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $dataset = [
                    'nome' => $_POST['name'],
                    'sku' => $_POST['sku'],
                    'preco' => $_POST['price'],
                    'descricao' => $_POST['description'],
                    'quantidade' => $_POST['quantity'],
                    'categoria' => implode('|', $_POST['category']),
                ];
                $product = new Products;
                if ($product->insert($dataset)) {
                    header('Location: ' . $_SERVER['HTTP_ORIGIN'] . '/product');
                } else {
                    $logger->error('Failed to insert product: ' . $dataset);
                }
            }

            if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
                \parse_str(file_get_contents("php://input"), $post_vars);
                $dataset = [];

                if (isset($post_vars['id'])) {
                    $dataset['id'] = $post_vars['id'];
                }
                if (isset($post_vars['sku'])) {
                    $dataset['sku'] = $post_vars['sku'];
                }
                if (isset($post_vars['name'])) {
                    $dataset['nome'] = $post_vars['name'];
                }
                if (isset($post_vars['price'])) {
                    $dataset['preco'] = $post_vars['price'];
                }
                if (isset($post_vars['description'])) {
                    $dataset['descricao'] = $post_vars['description'];
                }
                if (isset($post_vars['quantity'])) {
                    $dataset['quantidade'] = $post_vars['quantity'];
                }
                if (isset($post_vars['category'])) {
                    $dataset['categoria'] = implode('|', $post_vars['category']);
                }
                $product = new Products;

                if ($product->update($dataset)) {
                    header('Location: ' . $_SERVER['HTTP_ORIGIN'] . '/product');
                } else {
                    $logger->error('Failed to update product: ' . $dataset);
                }
            }

            if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
                \parse_str(file_get_contents("php://input"), $post_vars);

                if (!isset($post_vars['id'])) {
                    $logger->error('Missing product Id');
                }

                $product = new Products;

                if ($product->delete($post_vars['id'])) {
                    header('Location: ' . $_SERVER['HTTP_ORIGIN'] . '/categories');
                } else {
                    $logger->error('Failed to delete product with id: ' . $post_vars['id']);
                }
            }
        }

}
