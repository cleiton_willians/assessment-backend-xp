<?php

namespace Controller;

use Model\Products as Products;
use Model\Categories as Categories;
use View\CategoryListView as CategoryListView;
use View\AddCategoryView as AddCategoryView;
use Core\Logger as Logger;
/**
 *
 */
class CategoryController
{

    public function indexAction()
    {
        $categories = new Categories();
        $data = $categories->listAll();
        $view = new CategoryListView($data);
        $view->render();
    }

    public function addAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $view = new AddCategoryView();
            $view->render();
        }
        $logger = Logger::getLogger('application');

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $dataset = [
                'codigo' => $_POST['category-code'],
                'nome' => $_POST['category-name'],
            ];

            $category = new Categories();

            if ($category->insert($dataset)) {
                header('Location: ' . $_SERVER['HTTP_ORIGIN'] . '/categories');
            } else {
                $logger->error('Failed to insert category: ' . $dataset);
            }
        }

        if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
            \parse_str(file_get_contents("php://input"), $post_vars);
            $dataset = [];

            if (isset($post_vars['id'])) {
                $dataset['id'] = $post_vars['id'];
            }
            if (isset($post_vars['category-code'])) {
                $dataset['codigo'] = $post_vars['category-code'];
            }
            if (isset($post_vars['category-name'])) {
                $dataset['nome'] = $post_vars['category-name'];
            }
            $category = new Categories;

            if ($category->update($dataset)) {
                header('Location: ' . $_SERVER['HTTP_ORIGIN'] . '/categories');
            } else {
                $logger->error('Failed to update category: ' . $dataset);
            }
        }

        if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
            \parse_str(file_get_contents("php://input"), $post_vars);

            if (!isset($post_vars['id'])) {
                $logger->error('Missing category id');
            }

            $category = new Categories;

            if ($category->delete($post_vars['id'])) {
                header('Location: ' . $_SERVER['HTTP_ORIGIN'] . '/categories');
            } else {
                $logger->error('Failed to delete category with id: ' . $post_vars['id']);
            }
        }
    }

}
