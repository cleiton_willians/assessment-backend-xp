<?php

namespace Controller;

use Model\Products as Products;
use View\DashboardView as DashboardView;
/*
*
*/

class DashboardController
{

        public function indexAction()
        {
            $produtos = new Products();
            $data = $produtos->listAll();
            $dashView = new DashboardView($data);
            $dashView->render();
        }
}
