<?php

namespace Core;

/**
 *
 */
class Connection
{
    private static $conn;

    private function __construct()
    {
        // Avoid class instantiation
    }

    public static function getConnection()
    {
        if (is_null(self::$conn)) {
            $config = include(CONFIG_PATH . 'config.php');
            $dsn = "mysql:host={$config['mariadb']['host']};port={$config['mariadb']['port']};dbname={$config['mariadb']['dbname']}";
            self::$conn = new \PDO($dsn, $config['mariadb']['user'], $config['mariadb']['password']);
            self::$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            self::$conn->exec('SET NAMES utf8');
        }
        return self::$conn;
    }
}
