<?php

namespace Core;

use \Monolog\ErrorHandler as ErrorHandler;
use \Monolog\Handler\StreamHandler as StreamHandler;
/**
 *
 */
class Logger extends \Monolog\Logger
{
    private static $loggers = [];

    public function __construct($key = 'application')
    {
        parent::__construct($key);

        $config = [
            'logFile' => APP_PATH . "logs/{$key}.log",
            'logLevel' => \Monolog\Logger::DEBUG,
        ];

        $this->pushHandler(new StreamHandler($config['logFile'], $config['logLevel']));
    }

    public static function getLogger($key)
    {
        if (empty($loggers[$key])) {
            self::$loggers[] = new Logger($key);
        }

        return self::$loggers[$key];
    }

    public static function enableSystemLogs()
    {
        // Error Log
        self::$loggers['error'] = new Logger('errors');
        self::$loggers['error']->pushHandler(new StreamHandler(APP_PATH . "logs/errors.log"));
        ErrorHandler::register(self::$loggers['error']);

        // Request Log
        $data = [
            $_SERVER,
            $_REQUEST,
            trim(\file_get_contents("php://input")),
        ];
        self::$loggers['request'] = new Logger('request');
        self::$loggers['request']->pushHandler(new StreamHandler(APP_PATH . "logs/request.log"));
        self::$loggers['request']->info("REQUEST", $data);

        // Application Log
        self::$loggers['application'] = new Logger('application');
        self::$loggers['application']->pushHandler(new StreamHandler(APP_PATH . "logs/application.log"));
    }
}
