<?php

namespace Core;

use Controller\DashboardController as DashboardController;
use Controller\ProductController as ProductController;
use Controller\CategoryController as CategoryController;
use Core\Logger as Logger;
/**
 *
 */
class Application
{

    public static function run()
    {
        self::init();
        self::dispatch();
    }

    private static function init()
    {
        define('ROOT', getcwd() . '/');
        define('APP_PATH', ROOT . 'app/');
        define('PUBLIC_PATH', ROOT . 'public/');
        define('CONFIG_PATH', APP_PATH . 'config/');
        define('CONTROLLER_PATH', APP_PATH . 'controller/');
        define('MODEL_PATH', APP_PATH . 'model/');
        define('VIEW_PATH', APP_PATH . 'view/');
        Logger::enableSystemLogs();
    }

    private static function dispatch()
    {
        $router = new Router();
        $router->register(new Route('/dashboard/', 'DashboardController', 'indexAction'));
        $router->register(new Route('/product/', 'ProductController', 'indexAction'));
        $router->register(new Route('/categories/', 'CategoryController', 'indexAction'));
        $router->register(new Route('/addCategory/', 'CategoryController', 'addAction'));
        $router->register(new Route('/addProduct/', 'ProductController', 'addAction'));
        $router->handleRequest($_SERVER['REQUEST_URI']);
    }
}
